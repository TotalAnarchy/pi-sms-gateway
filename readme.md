# RaspberryPi as SMS Gateway

### **Very basic** SMS sending using a [RaspberryPi][rpi-link] and a 3G USB modem/dongle via a HTTP request.

### Usefull to integrate sms alerts on your existing applications.



> **Required materials:**
> ---
> * (1) RaspberryPi of your choice (I used a 3B that was laying around) & accessories as needed (charger/ethernet cable/etc)
> * (1) USB 3G/4G modem/dongle
> * (1) SIM card for the modem (recommended SIM card with SMS plan for obvious reasons)



> **Step 1: Basic install of *Raspbian Lite***
> ---
> (I used *Raspbian Lite* but should work with any distro of your choice)
> * Download latest *Raspbian Lite* version from [RaspberryPi website][raspbian-link]
> * Flash it to a SD card using [*Balena Etcher*][etcher-link] (or whatever flasher you prefer)
> * Create a blank file simply named ssh (no extension) on the boot folder of the SD card to enable SSH by default
> * Boot the pi with the new SD card
> * Find the IP (recommended to set a reservation on the router to always get the same IP) and SSH into it. (default user/pass is *`pi`/`raspberry`*)
> * Change *pi* user default password as sugested after console login
> * (*optional*) Run `sudo raspi-config` and change the hostname of the pi
> * Run `sudo apt update && sudo apt upgrade -y` to update your pi and packages to latest versions



> **Step 2: Connect and check your USB modem**
> ---
> * Connect the USB modem/dongle with the SIM card already inside
> * Run `lsusb` and check if the USB modem shows up (something like: *Bus 001 Device 006: ID 12d1:1001 Huawei Technologies Co., Ltd.*)
> * Run `dmesg | grep ttyUSB` to see where the modem is mounted.  
>   * Should look something like:
>   > usb 1-1.3.3: GSM modem (1-port) converter now attached to ttyUSB0  
>   > usb 1-1.3.3: GSM modem (1-port) converter now attached to ttyUSB1  
>   * You should/could take note of how many of these ttyUSB numbers you have because you may need them later for configuring *Gammu* (it's usually ttyUSB0)
>
> In my case both tested USB modems worked/showed up out-of-the-box (an old *Huawei USB E17X* and a slighty "newer" *ZTE MF637*), if not check the troubleshooting section below



> **Step 3: install [*Gammu*][gammu-link] and send test SMS via command line**
> ---
> * Run `sudo apt install -y gammu`
> * After installation completes, run `gammu-config`
> * Navigate the menu to change the settings, it should just be necessary to change the port to `/dev/ttyUSB0` leaving all others options as default
> * Hit save, a message should popup saying settings saved on *~/.gammurc*
> * Run `gammu identify` to check if *Gammu* detects your modem (if not run `gammu-config` again and try the next ttyUSB number, repeat)
> * Run `gammu sendsms TEXT 123456 -text "My 1st rpi SMS"`, where *123456* is your phone number
> * You should get a command line response saying SMS sent successfully, and receive the SMS on your phone soon after that



> **Step 4: HTTP "API"**
> ---
> * Install Apache and PHP, run `sudo apt install -y apache2 php`
> * After installation completes, set the correct server timezone on *php.ini* configuration file. Depending on your installation, new or existing, the location of the ini file may vary (usually `/etc/php/*php_version*/apache2/`).  
Run `sudo nano /etc/php/7.3/cli/php.ini` (in my case version 7.3 was installed), and look for the timezone line (*Ctrl+W* and type *timezone*), uncomment the line (remove ";" before the text *date.timezone =*), and add the correct timezone (see [PHP manual][php-timezone] to check available options)
>   * *(this is optional but setting the correct timezone makes our sms logs show up with the correct time values)*
> * Copy the initial config file *~/.gammurc* to your webserver/www-data homedir, run `cd ~/` followed by `sudo cp .gammurc /var/www/`
> * Add your webserver/www-data to dialout group, run `sudo gpasswd -a www-data dialout`
> * Create a sms.log file and allow your webserver/www-data to write to this file, run `sudo touch /var/www/html/sms.log` followed by `sudo chown www-data /var/www/html/sms.log`
> * Create a sms.php file, run `sudo nano /var/www/html/sms.php` and copy the code in the sms.php file on this repository (*or use git to clone, but you may need to apt install git for that*)
> * Test everything is working by opening your browser and navigating to `http://*your_pi_ip*/sms.php?number=*your_phone_number_here*&message=This is a sms from pi via http`
>   * If you want to add multiple numbers you have to separate them with ";"
>   * You can check the sms.log by navigating to `http://*your_pi_ip*/sms.log`



> **Troubleshooting:**
> ---
> * Pi doesn't recognize the USB modem (no ttyUSB modems appear)
>   * Follow the initial steps on the [original tutorial][reference-1] regarding the installation of the *usb-modeswitch* package
>
> * When calling the webpage to send the SMS it returns an error "Error opening device, you don't have permissions"
>   * make sure www-data user as permissions to group *dialout*, run `sudo gpasswd -a www-data dialout`
>   * and that www-data can access gammu, run `sudo chown root:gammu /usr/bin/gammu && sudo gpasswd -a www-data gammu`
> * My message got cut / didn't receive all the text
>   * to send a longer sms than 160 characters you need to add the parameter `-len` to the gammu command, `gammu sendsms TEXT 123456 -len 400 -text "Longer than normal sms text here"`, as used on sms.php
> * How do I send a sms with multiple lines?
>   * since we can't parse new lines on the url &message parameter if we need it we can use the code `%0A` to indicate a line break. Example `"This is line one.%0AThis is line two."`, this will generate a two line sms with the code being interpreted by the phone as a line break. *(tested on Android only)*
>



> Still to do (maybe):
> ---
> - [ ] PHP form page to send sms
> - [ ] refine PHP script
> - [ ] PHP page to view sent sms, or log
> - [ ] PHP page to view received sms (`gammu getallsms`) (and possibly delete them)
> - [ ] PHP whitelist numbers to execute commands on received sms
>


---
References used:
* [For setting up USB modem and *Gammu*][reference-1]
* [For the HTTP "API" to execute *Gammu* commands][reference-2]



[rpi-link]: https://www.raspberrypi.org
[raspbian-link]: https://www.raspberrypi.org/downloads/raspbian/
[etcher-link]: https://www.balena.io/etcher/
[gammu-link]: https://wammu.eu/gammu/
[php-timezone]: https://www.php.net/manual/en/timezones.php
[reference-1]: https://escapologybb.com/send-sms-from-raspberry-pi/
[reference-2]: https://github.com/uniquepurpose/smsphp
